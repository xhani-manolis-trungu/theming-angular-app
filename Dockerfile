FROM node:slim

WORKDIR /app
RUN apt-get update
RUN apt-get -y install

COPY . /app
RUN npm ci --quiet

CMD ["npm", "run", "start"]
