import { Component, OnInit } from '@angular/core';
import { MatIconRegistry } from '@angular/material/icon';
import { DomSanitizer } from '@angular/platform-browser';
import { Observable } from 'rxjs';

import { ThemeService } from '../core/services/theme.service';

@Component({
  selector: 'app-toolbar',
  templateUrl: './toolbar.component.html',
  styleUrls: ['./toolbar.component.scss'],
})
export class ToolbarComponent implements OnInit {
  checked = false;
  isDarkTheme: Observable<boolean>;

  constructor(
    private themeService: ThemeService,
    iconRegistry: MatIconRegistry,
    sanitizer: DomSanitizer
  ) {
    iconRegistry.addSvgIcon(
      'sun',
      sanitizer.bypassSecurityTrustResourceUrl(
        '../../assets/img/wb_sunny-24px.svg'
      )
    );
    iconRegistry.addSvgIcon(
      'night',
      sanitizer.bypassSecurityTrustResourceUrl(
        '../../assets/img/moon-waning-crescent.svg'
      )
    );
  }

  ngOnInit() {
    this.isDarkTheme = this.themeService.isDarkTheme;
  }

  toggleDarkTheme(checked: boolean) {
    this.checked = !this.checked;
    this.themeService.setDarkTheme(checked);
  }
}
