import { CommonModule } from '@angular/common';
import { HttpClientModule } from '@angular/common/http';
import { NgModule } from '@angular/core';
import { MatIconModule } from '@angular/material/icon';
import { MatSlideToggleModule } from '@angular/material/slide-toggle';
import { MatToolbarModule } from '@angular/material/toolbar';

import { FooterToolbarComponent } from './footer-toolbar.component';

@NgModule({
  declarations: [FooterToolbarComponent],
  imports: [
    CommonModule,
    MatToolbarModule,
    MatSlideToggleModule,
    MatIconModule,
    HttpClientModule,
  ],
  exports: [FooterToolbarComponent],
  bootstrap: [FooterToolbarComponent],
})
export class FooterToolbarModule {}
