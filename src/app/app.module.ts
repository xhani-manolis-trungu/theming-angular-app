import { NgModule } from '@angular/core';
import { MatIconModule } from '@angular/material/icon';
import { MatSidenavModule } from '@angular/material/sidenav';
import { BrowserModule } from '@angular/platform-browser';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { ThemeService } from './core/services/theme.service';
import { FooterToolbarModule } from './footer-toolbar/footer-toolbar.module';
import { TileComponent } from './tile/tile.component';
import { ToolbarModule } from './toolbar/toolbar.module';
import { TreeComponent } from './tree.component';
import { MatTreeModule } from '@angular/material/tree';
import { MatButtonModule } from '@angular/material/button';

@NgModule({
  declarations: [AppComponent, TileComponent, TreeComponent],
  imports: [
    BrowserModule,
    AppRoutingModule,
    BrowserAnimationsModule,
    MatIconModule,
    MatSidenavModule,
    ToolbarModule,
    FooterToolbarModule,
    MatTreeModule,
    MatButtonModule,
  ],
  providers: [ThemeService],
  bootstrap: [AppComponent],
  exports: [TreeComponent],
  entryComponents: [TreeComponent],
})
export class AppModule {}
