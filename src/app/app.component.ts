import { Component, OnInit } from '@angular/core';
import { gsap } from 'gsap';
import { Observable } from 'rxjs';

import { ThemeService } from './core/services/theme.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss'],
})
export class AppComponent implements OnInit {
  public title: 'Theme Demo';
  isDarkTheme: Observable<boolean>;

  constructor(private themeService: ThemeService) {}

  ngOnInit() {
    // gsap.from(".mat-toolbar-row", { duration: 2, x: 200, opacity: 0.3, scale: 0.3 });
    gsap.from('.content', { duration: 3, x: 300, opacity: 0, scale: 0.5 });

    this.isDarkTheme = this.themeService.isDarkTheme;

    // Sticky Header Function addStickyClass

    // window.onscroll = function() {addStikcyClass()};

    // // Get the header
    // var header = document.getElementById("myHeader");

    // // Get the offset position of the navbar
    // var sticky = header.offsetTop;

    // // Add the sticky class to the header when you reach its scroll position. Remove "sticky" when you leave the scroll position
    // function addStikcyClass() {
    //   if (window.pageYOffset > sticky) {
    //     header.classList.add("sticky");
    //   } else {
    //     header.classList.remove("sticky");
    //   }
    // }
  }
}
